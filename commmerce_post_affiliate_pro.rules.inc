<?php
/*
 * @file commmerce_post_affiliate_pro.rules.inc
 * Provides rules actions/integration
 * @copyright Copyright(c) 2011 Rowlands Group
 * @license GPL v3 http://www.fsf.org/licensing/licenses/gpl.html
 * @author Lee Rowlands leerowlands at rowlandsgroup dot com
 * 
 */

/**
 * Implements hook_rules_action_info();
*/
function commerce_post_affiliate_pro_rules_action_info() {
  $actions = array();

  $actions['commerce_post_affiliate_pro_sales_integration'] = array(
    'label' => t('Notify the PAP server of a sale.'),
    'parameter' => array(
      'commerce_order' => array(
        'type' => 'commerce_order',
        'label' => t('Order to notify'),
      ),
    ),
    'group' => t('Commerce Order'),
    'callbacks' => array(
      'execute' => 'commerce_post_affiliate_pro_sales_notify',
    ),
  );
}

/**
 * Rules action: Notifies PAP of a sale
 */
function commerce_post_affiliate_pro_sales_notify($order) {
  $order_wrapper = entity_metadata_wrapper('commerce_order', $order);
  dsm($order);
  $pap_session_merchant = new Drupal_Gpf_Api_Session(variable_get('commerce_post_affiliate_pro_merchant_url_prefix', 'http://demo.qualityunit.com/pax4/'). "scripts/sale.php");

  $sale_tracker = new Pap_Api_SaleTracker($pap_session_merchant);
  
  $sale_tracker->setAccountId('default1');
  
  $sale = $sale_tracker->createSale();
  $sale->setTotalCost($order->order_total / 100);
  $sale->setOrderID($order->order_id);
  
  $sale_tracker->register(); 
}